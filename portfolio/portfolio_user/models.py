from django.db import models

class PortfolioUser(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    dob = models.DateField()
    linkedin_profile_url = models.URLField(null=True, blank=True)
    github_profile_url = models.URLField(null=True, blank=True)
    email = models.EmailField()
    phone = models.CharField(max_length=12)
    starter_description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.first_name + self.last_name


class UserSkills(models.Model):
    name = models.CharField(max_length=200)
    logo = models.FileField(null=True, blank=True)
    user = models.ManyToManyField(PortfolioUser, related_name="user_skills")

    def __str__(self):
        return self.name

class UserEducationDetails(models.Model):
    institute_name = models.CharField(max_length=200)
    start_date = models.DateField()
    end_date = models.DateField()
    degree_name = models.CharField(max_length=200)
    user = models.ForeignKey(
            PortfolioUser,
            related_name='user_education',
            on_delete=models.CASCADE
        )

    def __str__(self):
        return self.institute_name


class UserAchievement(models.Model):
    description = models.TextField()
    user = models.ForeignKey(
            PortfolioUser,
            related_name='user_achievement',
            on_delete=models.CASCADE
        )



class UserExperience(models.Model):
    company_name = models.CharField(max_length=200)
    position = models.CharField(max_length=200)
    start_date = models.DateField()
    end_date= models.DateField(blank=True, null=True)
    description = models.TextField()
    user = models.ForeignKey(
            PortfolioUser,
            related_name='user_experience',
            on_delete=models.CASCADE
        )

    def __str__(self):
        return self.company_name + ' - ' + self.position

class UserProjects(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)
    user_experience = models.ForeignKey(
                        UserExperience,
                        related_name='user_experience_projects',
                        on_delete=models.CASCADE
                )

    def __str__(self):
        return self.name

class UserProjectResponsibilities(models.Model):
    description = models.TextField()
    user_project = models.ForeignKey(
                    UserProjects,
                    related_name='user_project_reponsibilites',
                    on_delete=models.CASCADE
                )

class UserComment(models.Model):
    comment = models.TextField()