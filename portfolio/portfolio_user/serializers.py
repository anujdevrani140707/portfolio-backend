from rest_framework import serializers
from . import models as portfolio_user_models


class UserEducationSerializer(serializers.ModelSerializer):

    class Meta:
        model = portfolio_user_models.UserEducationDetails
        fields = ['institute_name', 'start_date', 'end_date', 'degree_name']


class UserAchievementSerializer(serializers.ModelSerializer):

    class Meta:
        model = portfolio_user_models.UserAchievement
        fields = '__all__'


class UserProjectsResponsibilitySerializer(serializers.ModelSerializer):

    class Meta:
        model = portfolio_user_models.UserProjectResponsibilities
        fields = '__all__'


class UserProjectsSerializer(serializers.ModelSerializer):
    user_project_reponsibilites = UserProjectsResponsibilitySerializer(many=True)
    class Meta:
        model = portfolio_user_models.UserProjects
        fields = '__all__'


class UserExperienceSerializer(serializers.ModelSerializer):
    user_experience_projects = UserProjectsSerializer(many=True)
    class Meta:
        model = portfolio_user_models.UserExperience
        fields = '__all__'

class UserSkillsSerializer(serializers.ModelSerializer):
    class Meta:
        model = portfolio_user_models.UserSkills
        fields = '__all__'


class UserPortfolioSerializer(serializers.ModelSerializer):
    user_education = UserEducationSerializer(many=True)
    user_achievement = UserAchievementSerializer(many=True)
    user_experience = UserExperienceSerializer(many=True)
    user_skills = UserSkillsSerializer(many=True)

    class Meta:
        model = portfolio_user_models.PortfolioUser
        fields = '__all__'


class UserCommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = portfolio_user_models.UserComment
        fields = '__all__'