from drf_yasg.utils import swagger_auto_schema
from . import serializers as portfolio_serializer
from . import models as portfolio_models
from rest_framework import status, views, viewsets
from rest_framework.response import Response


class GetPortfolioDetails(views.APIView):
    serializer_class = portfolio_serializer.UserPortfolioSerializer

    @swagger_auto_schema(responses = {
        200: portfolio_serializer.UserPortfolioSerializer()
        }
    )
    def get(self, request):
        # Since we have only one user get the details for the user

        portfolio_user = portfolio_models.PortfolioUser.objects.get(id=1)

        serialized_data = self.serializer_class(portfolio_user)

        return Response(serialized_data.data, status=status.HTTP_200_OK)


class CommentAPI(viewsets.ModelViewSet):
    serializer_class = portfolio_serializer.UserCommentSerializer
    queryset = portfolio_models.UserComment.objects.all().order_by('-id')