from django.contrib import admin
from portfolio.portfolio_user import models as portfolio_models

admin.site.register(portfolio_models.PortfolioUser)
admin.site.register(portfolio_models.UserAchievement)
admin.site.register(portfolio_models.UserEducationDetails)
admin.site.register(portfolio_models.UserProjects)
admin.site.register(portfolio_models.UserProjectResponsibilities)
admin.site.register(portfolio_models.UserExperience)
admin.site.register(portfolio_models.UserSkills)
