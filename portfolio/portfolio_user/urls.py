from django.urls import path, include
from . import apis as apis
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'comment', apis.CommentAPI)

urlpatterns = [
    path('v1/getportfoliodata/',
         apis.GetPortfolioDetails.as_view(),
         name="portfoliodata"
         ),

     path('v1/', include(router.urls)),
    ]
