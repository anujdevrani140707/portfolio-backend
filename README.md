# Portfolio Project
A python project to showcase my portfolio

# Prerequisites
python 3.8 virtual environment

# Local Setup

Make sure you have all the prerequisites.

1. Pip install all the requirements.
	```python
	pip install -r requirements.txt
	```

2. Start the dev server for local development:
	```python
	python manage.py runserver
	```
3. Navigate to /swagger for docs.
	
    Example: http://localhost:8000/swagger/ 