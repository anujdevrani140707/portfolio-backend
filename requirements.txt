# Core
pytz==2020.1
Django==3.0.8
django-configurations==2.2

# Rest apis
djangorestframework==3.11.0
Markdown==3.2.2
django-filter==2.3.0

# Developer Tools
ipdb==0.13.3
ipython==7.16.1
mkdocs==1.1.2
flake8==3.8.3

# Static and Media Storage
django-storages==1.9.1
boto3==1.14.16

# Documentation
drf-yasg==1.17.1
